<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use Symfony\Bundle\FrameworkBundle\Controller;

class LoginController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('login/index.html.twig');
    }
}